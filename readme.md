# Page Replacement Algorithm Simulator


A client based standalone application built on java.

## How it works?
    Page Replacement Algorithm Simulator works this way:
        1. Enter frame size (eg. 3)
        2. Enter pages (eg. 4, 7, 6, 1, 7, 6, 1, 2, 7, 2)
        3. Select Algorithm (eg. Optimal)
    

## ScreenShots

### FIFO Algorithm

![FIFO Algorithm](screenshots/fifo.png)

### LRU Algorithm

![LRU Algorithm](screenshots/lru.png)

### Optimal Algorithm 

![Optimal Algorithm](screenshots/optimal.png)