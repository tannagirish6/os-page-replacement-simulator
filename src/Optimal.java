
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
public class Optimal {
    private int hits;
    private float hitRatio;
    void generatePages(int frameSize, ArrayList<Integer> pagesList, JPanel displayPagePanel){
        int frame[] = new int[frameSize], temp[] = new int[frameSize], flag1, flag2, flag3, index=0, max, hits = 0;
        
        for(int i = 0; i < frameSize; i++){
            frame[i] = -1;
        }
        for(int i=0; i<pagesList.size(); i++){
            flag1 = flag2 = 0;
            for(int j = 0; j < frameSize; ++j){
                if(frame[j] == pagesList.get(i)){
                    flag1 = flag2 = 1;
                    generateFrame(frame, displayPagePanel, pagesList.get(i) + " ( Hit ) ", pagesList);
                    hits++;
                    break;
                }
            }
            if(flag1 == 0){
                for(int j=0; j<frameSize; ++j){
                    if(frame[j] == -1){
                        frame[j] = pagesList.get(i);
                        flag2 = 1;
                        generateFrame(frame, displayPagePanel, pagesList.get(i) + " ( Miss ) ", pagesList);
                        break;
                    }
                }    
            }
            if(flag2 == 0){
        	flag3 = 0;
                for(int j=0; j<frameSize; ++j){
                    temp[j] = -1;

                    for(int k=i+1; k<pagesList.size(); ++k){
                        if(frame[j] == pagesList.get(k)){
                            temp[j]=k;
                            break;
                        }
                    }
                }
                for(int j=0; j<frameSize; ++j){
                    if(temp[j] == -1){
                        index = j;
                        flag3 = 1;
                        break;
                    }
                }
                if(flag3 ==0){
                    max = temp[0];
                    index = 0;

                    for(int j=1; j<frameSize; ++j){
                        if(temp[j] > max){
                            max = temp[j];
                            index = j;
                        }
                    }            	
                }
			
                frame[index] = pagesList.get(i);
                generateFrame(frame, displayPagePanel, pagesList.get(i) + " ( Miss ) ", pagesList);
            }
//            System.out.println();
//        
//            for(int j=0; j<frameSize; ++j){
//                System.out.println("\t " + frame[j]);
//            }
        }
        this.hits = hits;
        this.hitRatio = (float)(((float)this.hits/(float)pagesList.size())*100.0f);
    }
    private void generateFrame(int[] frame, JPanel displayPagePanel, String status, ArrayList<Integer> pagesList) {
        displayPagePanel.setLayout(new GridLayout(pagesList.size()/5, 5));
        
        PageFrame pf = new PageFrame(frame, status);
        displayPagePanel.add(pf);
        
        pf.validate();
        pf.updateUI();
    }
    int getHits(){
        return this.hits;
    }
    float getHitRatio(){
        return this.hitRatio;
    }
}
