import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
public class FIFO {
    private int hits;
    private float hitRatio;
    void generatePages(int frameSize, ArrayList<Integer> pagesList, JPanel displayPagePanel){
        int pageHits = 0, pointer = 0;
        int frame[] = new int[frameSize];
        // Flag: true - Page Miss; false - Page Hit
        boolean flag = false;
        for(int i=0; i<frameSize; i++)
            frame[i] = -1;
        for(int i=0; i<pagesList.size(); i++){
            flag = true;
            int page = pagesList.get(i);
            for(int j=0; j<frameSize; j++){
                if(frame[j] == page){
                    flag = false;
                    pageHits++;
                    break;
                }
            }
            if(pointer == frameSize)
                pointer = 0;
            if(flag){
                // Page Miss!
                frame[pointer] = page;
                pointer++;
            }else{
                // Page Hit!
            }
            if(flag)
                generateFrame(frame, displayPagePanel, pagesList.get(i) + " ( Miss ) ", pagesList);
            else
                generateFrame(frame, displayPagePanel, pagesList.get(i) + " ( Hit ) ", pagesList);
        }
//        System.out.println("Number Of Page Hits: " + pageHits);
        this.hits = pageHits;
        this.hitRatio = (float)(((float)this.hits/(float)pagesList.size())*100.0f);
    }

    private void generateFrame(int[] frame, JPanel displayPagePanel, String status, ArrayList<Integer> pagesList) {
        displayPagePanel.setLayout(new GridLayout(pagesList.size()/5, 5));
        
        PageFrame pf = new PageFrame(frame, status);
        displayPagePanel.add(pf);
        
        pf.validate();
        pf.updateUI();
    }
    int getHits(){
        return this.hits;
    }
    float getHitRatio(){
        return this.hitRatio;
    }
}
